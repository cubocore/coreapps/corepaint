project( corepaint )
cmake_minimum_required( VERSION 3.16 )

set( PROJECT_VERSION 5.0.0 )
set( PROJECT_VERSION_MAJOR 5 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

find_package( Qt6Core REQUIRED )
find_package( Qt6Gui REQUIRED )
find_package( Qt6Widgets REQUIRED )

set( SOURCES
    src/dialogs/resizedialog.cpp
    src/dialogs/resizedialog.h
    src/dialogs/textdialog.cpp
    src/dialogs/textdialog.h
    src/instruments/abstractinstrument.cpp
    src/instruments/abstractinstrument.h
    src/instruments/abstractselection.cpp
    src/instruments/abstractselection.h
    src/instruments/colorpickerinstrument.cpp
    src/instruments/colorpickerinstrument.h
    src/instruments/curvelineinstrument.cpp
    src/instruments/curvelineinstrument.h
    src/instruments/ellipseinstrument.cpp
    src/instruments/ellipseinstrument.h
    src/instruments/eraserinstrument.cpp
    src/instruments/eraserinstrument.h
    src/instruments/fillinstrument.cpp
    src/instruments/fillinstrument.h
    src/instruments/lineinstrument.cpp
    src/instruments/lineinstrument.h
    src/instruments/pencilinstrument.cpp
    src/instruments/pencilinstrument.h
    src/instruments/rectangleinstrument.cpp
    src/instruments/rectangleinstrument.h
    src/instruments/selectioninstrument.cpp
    src/instruments/selectioninstrument.h
    src/instruments/sprayinstrument.cpp
    src/instruments/sprayinstrument.h
    src/instruments/textinstrument.cpp
    src/instruments/textinstrument.h
    src/widgets/colorchooser.cpp
    src/widgets/colorchooser.h
    src/additionaltools.cpp
    src/additionaltools.h
    src/corepaint.cpp
    src/corepaint.h
    src/datasingleton.cpp
    src/datasingleton.h
    src/imagearea.cpp
    src/imagearea.h
    src/undocommand.cpp
    src/undocommand.h
    src/settings.cpp
    src/settings.h
    src/main.cpp
)

set( UIS
    src/corepaint.ui
)

set ( RSCS
    icons.qrc
)

add_executable( corepaint ${RSCS} ${SOURCES} ${UIS}  )
target_link_libraries ( corepaint  Qt6::Core Qt6::Gui Qt6::Widgets cprime-widgets cprime-core )

install( TARGETS corepaint DESTINATION bin )
install( FILES cc.cubocore.CorePaint.desktop DESTINATION share/applications )
install( FILES cc.cubocore.CorePaint.svg DESTINATION share/icons/hicolor/scalable/apps/ )
